require("./bootstrap");

import Vue from "vue";
import Root from "./components/Root";

new Vue({
    el: "#app",
    components: { Root }
});
