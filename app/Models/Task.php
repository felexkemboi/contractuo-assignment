<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'tasks';

    protected $fillable = ['description'];
}
