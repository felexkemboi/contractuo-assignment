<p align="center"><a href="https://gitlab.com/contractuo_public/assignment-project" target="_blank"><img src="https://www.contractuo.com/wp-content/uploads/2022/03/cropped-Contractuo-Logo.png" width="400"></a></p>

## The assignment

### Prerequisites

Below you’ll find some directions to setup the development environment and project.

1. Setup your development environment;

    - IDE of you choosing
    - PHP 8+
    - Composer
    - Node.JS

2. Checkout the current repository, install the dependencies with the following commands:

    - composer install
    - npm install

### Project details

The application is a basic Laravel application with Vuejs.

In terms of UI, we have installed both:

-   <a href="https://tailwindcss.com/" target="_blank">Tailwindcss</a>
-   <a href="https://getbootstrap.com/docs/4.6/getting-started/introduction/" target="_blank">Bootstrap</a>

Choose the one that you prefer.

### Goal of the assignment

-   The user needs to be able to create a single Todo daily entry.

### Requirements

-   Add a new Vue component inside the main Root.vue component.
-   The component needs to provide an input for the user to create the todo entry.
-   Add the corresponding backend logic to save a single todo entry into the database.
-   No relation between the user and the todo is required.
